import { DialogType } from "../Utils/DialogController";
import GlobalController from "../Utils/GlobalController";
import Effect from "../Utils/Effect";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {

    @property(cc.Button)
    playButton: cc.Button = null

    @property(cc.Button)
    settingsButton: cc.Button = null

    @property(cc.Button)
    resultsButton: cc.Button = null

    @property(cc.Sprite)
    line: cc.Sprite = null

    @property(cc.Node)
    newLogo: cc.Node = null

    @property(cc.Animation)
    pinkBalloon: cc.Animation = null

    onLoad() {
        this.startAnimation()
    }

    private startAnimation() {
        let actions = new Array<cc.FiniteTimeAction>(cc.delayTime(0.5))
        let dt = 0.025
        for (let i = 0; i < 1 / dt; i ++) { actions.push(cc.callFunc(() => this.line.fillRange += dt), cc.delayTime(dt)) }
        actions.push(cc.callFunc(() => this.destroyPinkBalloon()))
        this.node.runAction(cc.sequence(actions))
    }

    private destroyPinkBalloon() {
        let anim = cc.callFunc(() => {
            this.pinkBalloon.play()
            this.pinkBalloon.node.getComponent(Effect).playEffect()
        })
        let delay = cc.delayTime(0.2)
        let remove = cc.removeSelf()
        let seq = cc.sequence(anim, delay, remove, cc.callFunc(() => this.showNewLogo()))
        this.pinkBalloon.node.runAction(seq)
    }

    private showNewLogo() {
        let move = cc.moveTo(2, cc.p(this.newLogo.position.x, cc.winSize.height / 2))
        let seq = cc.sequence(move, cc.callFunc(() => this.showMenuButtons()))
        this.newLogo.runAction(seq)
    }

    private showMenuButtons() {
        let fadeIn = cc.fadeIn(1)
        this.playButton.interactable = true
        this.settingsButton.interactable = true
        this.resultsButton.interactable = true
        this.playButton.node.runAction(fadeIn)
        this.settingsButton.node.runAction(fadeIn.clone())
        this.resultsButton.node.runAction(fadeIn.clone())
    }

    playButtonHandler() {
        cc.director.loadScene("Game");
    }

    showSettings() { GlobalController.dc.showDialog(DialogType.SettingsDialog) }
    showResults() { GlobalController.dc.showDialog(DialogType.ResultsDialog) }
}