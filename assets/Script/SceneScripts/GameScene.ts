import Balloon from "../Core/Balloon";
import GlobalController from "../Utils/GlobalController";
import Effect from "../Utils/Effect";
import { DialogType } from "../Utils/DialogController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameScene extends cc.Component {

    @property(cc.Label)
    scoreLabel: cc.Label = null

    @property(cc.Label)
    lifesLabel: cc.Label = null

    @property(cc.Prefab)
    balloonPrefab: cc.Prefab = null

    @property(cc.Node)
    balloonsNode: cc.Node = null

    @property(Effect)
    balloonDestroySound: Effect = null

    createBalloon() {
        let getRandomBalloonPosX = (balloonWidth) => { return Math.floor(Math.random() * (cc.winSize.width - balloonWidth)) - cc.winSize.width / 2 + balloonWidth / 2 }

        let balloon = cc.instantiate(this.balloonPrefab)
        this.balloonsNode.addChild(balloon)

        let screenHeight = cc.winSize.height

        let balloonPosX = getRandomBalloonPosX(balloon.width)
        balloon.setPosition(balloonPosX, -screenHeight / 2 - balloon.height)
        
        let bezierPath = [cc.p(getRandomBalloonPosX(balloon.width), Math.floor(Math.random() * -screenHeight / 2) + screenHeight / 4), 
            cc.p(getRandomBalloonPosX(balloon.width), Math.floor(Math.random() * screenHeight / 2) - screenHeight / 4), 
            cc.p(getRandomBalloonPosX(balloon.width), screenHeight / 2 + balloon.height)]
        let speed = Math.floor(Math.random() * 4) + balloon.getComponent(Balloon).baseSpeed
        let bezMove = cc.bezierTo(speed, bezierPath)

        let remove = cc.removeSelf()
        let finish = cc.callFunc(() => GlobalController.gameModel.lossLife())
        let seq = cc.sequence(bezMove, finish, remove)
        balloon.runAction(seq)
    }

    onLoad () {
        GlobalController.gameModel.gameScene = this
        GlobalController.gameModel.beginGame()
    }

    updateLifes(lifeValue) {
        this.lifesLabel.string = lifeValue.toString()
    }

    updateScore(scoreValue) {
        this.scoreLabel.string = scoreValue.toString()
    }

    resetScene() {
        this.unschedule(this.createBalloon)
        this.balloonsNode.removeAllChildren()
    }

    showFinish(score) {
        this.resetScene()
        GlobalController.dc.showDialog(DialogType.FinishDialog, score)
    }

    showPause() {
        GlobalController.dc.showDialog(DialogType.PauseDialog)
    }
}