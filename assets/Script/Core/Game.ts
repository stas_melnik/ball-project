import GameScene from "../SceneScripts/GameScene";

export default class Game {

    private _gameScene: GameScene = null
    public set gameScene(gameScene) { this._gameScene = gameScene }
    public get gameScene() { return this._gameScene }
    
    private _score = 0
    private _lifes = 3
    private _minGenerateTime = 0.3
    private _maxGenerateTime = 0.5

    public beginGame() {
        this._score = 0
        this._lifes = 3
        this._gameScene.updateLifes(this._lifes)
        this._gameScene.updateScore(this._score)
        this.runGenerator()
    }

    private runGenerator() {
        this._gameScene.schedule(this._gameScene.createBalloon, (Math.random() * (this._maxGenerateTime - this._minGenerateTime)) + this._minGenerateTime)
    }

    public lossLife() {
        this._gameScene.updateLifes(--this._lifes)
        if (this._lifes <= 0) this._gameScene.showFinish(this._score)
    }

    public balloonDestroyed() {
        this._gameScene.updateScore(++this._score)
        this._gameScene.balloonDestroySound.playEffect()
    }
}
