import GlobalController from "../Utils/GlobalController";

const {ccclass} = cc._decorator;

@ccclass
export default class Balloon extends cc.Component {

    baseSpeed = 4

    onLoad () {
        let r = Math.floor(Math.random() * 255)
        let g = Math.floor(Math.random() * 255)
        let b = Math.floor(Math.random() * 255)
        this.node.color = cc.color(r, g, b)

        this.node.on(cc.Node.EventType.TOUCH_START, this.boom, this, true)
    }

    boom() {
        this.node.stopAllActions()
        let scale = cc.scaleTo(0.2, 0, 0)
        let destroy = cc.removeSelf()
        let seq = cc.sequence(scale, destroy)
        this.node.runAction(seq)
        GlobalController.gameModel.balloonDestroyed()
        this.node.off(cc.Node.EventType.TOUCH_START, this.boom, this)
    }
}