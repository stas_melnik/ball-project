import AuidioController from "./AudioController";

const {ccclass} = cc._decorator;

@ccclass
export default class Effect extends cc.AudioSource {
    playEffect() {
        AuidioController.playEffect(this)
    }
}