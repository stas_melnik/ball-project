import Music from "./Music";

export default class AuidioController {

    private static _muteEffects: boolean = JSON.parse(cc.sys.localStorage.getItem("SoundMute"))
    private static _muteMusic: boolean = JSON.parse(cc.sys.localStorage.getItem("MusicMute"))
    private static _currentMusic: Music = null

    static set muteEffects(value) {
        this._muteEffects = value
        cc.sys.localStorage.setItem("SoundMute", value)
    }

    static set muteMusic(value) {
        this._muteMusic = value
        if (this._currentMusic) this._currentMusic.volume = value ? 0 : 1
        cc.sys.localStorage.setItem("MusicMute", value)
    }

    static get muteEffects() { return this._muteEffects }
    static get muteMusic() { return this._muteMusic }

    static playEffect(effect) {
        if (!this._muteEffects) effect.play()
    }

    static playMusic(music) {
        this._currentMusic = music
        music.volume = this._muteMusic ? 0 : 1
        music.play()
    }

}