import Game from "../Core/Game";
import UserResults from "../UserResults/UserResults";
import DialogController from "./DialogController";

export default class GlobalController {
    private static _instance: GlobalController = null
    public static get instance() { return this._instance || (this._instance = new this()) }

    private _gameModel = new Game()
    static get gameModel() { return this.instance._gameModel }

    private _userResults = new UserResults()
    static get userResults() { return this.instance._userResults }

    private _dc: DialogController = new DialogController()
    static get dc() { return this.instance._dc }
}