export enum DialogType {
    SettingsDialog = "SettingsDialog",
    ResultsDialog = "ResultsDialog",
    PauseDialog = "PauseDialog",
    FinishDialog = "FinishDialog"
}

export default class DialogController {
    showDialog(type: DialogType, ...par) {
        cc.loader.loadRes("Dialogs/" + type.toString(), cc.Prefab, (err, prefab) => {
            if(err) {
                cc.error(err.message || err)
                return
            }
            let component = type.toString()
            let cb = (type == DialogType.FinishDialog) ? (d) => d.getComponent(component).setScore(par) : () => {}
            let dialog = cc.instantiate(prefab)
            cc.director.getScene().addChild(dialog)
            dialog.getComponent(component).showDialog(cb)
        })
    }
}