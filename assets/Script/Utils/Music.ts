import AuidioController from "./AudioController";

const {ccclass} = cc._decorator;

@ccclass
export default class Music extends cc.AudioSource {
    onLoad() {
        AuidioController.playMusic(this)
    }
}