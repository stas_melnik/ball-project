import Dialog from "./Dialog";
import ResultsCell from "../UserResults/ResultsCell";
import GlobalController from "../Utils/GlobalController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ResultsDialog extends Dialog {

    @property(cc.Prefab)
    cellPrefab: cc.Prefab = null

    @property(cc.Node)
    content: cc.Node = null

    onLoad() {
        this.renderTable()
    }

    showDialog(cb = null) {
        let fade = cc.fadeTo(this.animationTime, this.fadeOpacity)
        this.fade.runAction(fade)
    }

    hideDialog() {
        let fadeOut = cc.fadeOut(this.animationTime)
        let remove = cc.removeSelf()
        let seq = cc.sequence(fadeOut, remove)
        this.fade.runAction(seq)
    }

    renderTable() {
        for(let i = 0; i < GlobalController.userResults.maxResutls; i++) {
            let cell = cc.instantiate(this.cellPrefab)
            this.content.addChild(cell)
            let resCell = cell.getComponent(ResultsCell);
            resCell.nameLabel.string = GlobalController.userResults.results[i].name
            resCell.scoreLabel.string = GlobalController.userResults.results[i].score.toString()
        }
    }
}
