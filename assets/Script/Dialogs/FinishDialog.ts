import Dialog from "./Dialog";
import GlobalController from "../Utils/GlobalController";
import Effect from "../Utils/Effect";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FinishDialog extends Dialog {

    @property(cc.Label)
    scoreLabel: cc.Label = null

    @property(cc.Node)
    boxBack: cc.Node = null

    @property(cc.EditBox)
    resultBox: cc.EditBox = null

    @property(cc.Button)
    saveButton: cc.Button = null

    @property(cc.Label)
    successLabel: cc.Label = null

    private score: number

    showDialog(cb) {
        super.showDialog(cb)
        this.node.getComponent(Effect).playEffect()
    }

    menuButtonHandler() {
        cc.director.loadScene("Menu")
    }

    replayButtonHandler() {
        this.node.destroy()
        GlobalController.gameModel.beginGame()
    }

    checkNameLength() {
        if(this.resultBox.string.length >= 3) {
            this.saveButton.interactable = true
        }
    }

    saveResultButtonHandler() {
        GlobalController.userResults.newResult(this.resultBox.string, this.score)
        this.boxBack.active = false
        this.resultBox.node.active = false
        this.saveButton.node.active = false
        this.successLabel.node.active = true
    }

    setScore(score) {
        this.scoreLabel.string = "Your score: " + score
        this.score = score

        if(GlobalController.userResults.checkRecord(score)) {
            this.boxBack.active = true
            this.resultBox.node.active = true
            this.saveButton.node.active = true
        } else {
            this.scoreLabel.node.position = cc.p()
        }
    }
}