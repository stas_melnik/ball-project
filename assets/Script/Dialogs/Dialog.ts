const {ccclass, property} = cc._decorator;

@ccclass
export default class Dialog extends cc.Component {

    @property(cc.Node)
    fade: cc.Node = null

    @property(cc.Node)
    back: cc.Node = null

    @property
    fadeOpacity: number = 130

    @property
    animationTime = 0.5

    @property
    needToPause: boolean = false

    showDialog(cb) {
        let fade = cc.fadeTo(this.animationTime, this.fadeOpacity)
        this.fade.runAction(fade)
        let move = cc.moveTo(this.animationTime, cc.p())
        this.back.runAction(move)
        if (this.needToPause) {
            let invokePause = () => { cc.game.pause() }
            this.scheduleOnce(invokePause, this.animationTime + 0.01)
        }
        cb(this)
    }

    hideDialog() {
        let fadeOut = cc.fadeOut(this.animationTime)
        let remove = cc.removeSelf()
        let seq = cc.sequence(fadeOut, remove)
        this.fade.runAction(seq)
        let topPos = cc.winSize.height / 2 + this.back.height / 2
        let move = cc.moveTo(this.animationTime, cc.p(0, topPos))
        this.back.runAction(move)
        if (this.needToPause) cc.game.resume()
    }
}
