import Dialog from "./Dialog";
import GlobalController from "../Utils/GlobalController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PauseDialog extends Dialog {

    menuButtonHandler() {
        cc.game.resume()
        cc.director.loadScene("Menu")
    }

    replayButtonHandler() {
        cc.game.resume()
        this.node.destroy()
        GlobalController.gameModel.gameScene.resetScene()
        GlobalController.gameModel.beginGame()
    }

}
