import Dialog from "./Dialog"
import AuidioController from "../Utils/AudioController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SettingsDialog extends Dialog {

    @property(cc.Toggle)
    musicToggle: cc.Toggle = null

    @property(cc.Toggle)
    soundToggle: cc.Toggle = null

    onLoad() {
        this.musicToggle.isChecked = AuidioController.muteMusic
        this.soundToggle.isChecked = AuidioController.muteEffects
    }

    musicToggleHandler() {
        AuidioController.muteMusic = !AuidioController.muteMusic
    }

    soundToggleHandler() {
        AuidioController.muteEffects = !AuidioController.muteEffects
    }
}
