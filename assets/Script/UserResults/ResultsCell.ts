const {ccclass, property} = cc._decorator;

@ccclass
export default class ResultsCell extends cc.Component {
    @property(cc.Label)
    nameLabel: cc.Label = null

    @property(cc.Label)
    scoreLabel: cc.Label = null
}