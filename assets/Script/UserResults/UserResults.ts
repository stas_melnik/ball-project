interface ResultData {
    name: string
    score: number
}

export default class UserResults {

    results: ResultData[] = []
    maxResutls: number = 10

    constructor() {
        this.results = JSON.parse(cc.sys.localStorage.getItem("UserResults"))
        if (this.results == null) this.firstLaunchInit()
    }
    
    checkRecord(score) {
        return score > this.results[this.maxResutls - 1].score
    }

    newResult(name, score) {
        this.results[this.maxResutls - 1] = { name: name, score: score }
        this.results = this.results.sort((res1, res2) => res2.score - res1.score)
        this.saveResults()
    }

    private saveResults() {
        cc.sys.localStorage.setItem("UserResults", JSON.stringify(this.results))
    }

    private firstLaunchInit() {
        this.results = []
        for(let i = 0; i < this.maxResutls; i++) this.results.push({name: "x", score: 0})
        this.saveResults()
    }
}